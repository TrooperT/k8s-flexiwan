# This file is a template, and might need editing before it works on your project.
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Getting-Started.gitlab-ci.yml

# This is a sample GitLab CI/CD configuration file that should run without any modifications.
# It demonstrates a basic 3 stage CI/CD pipeline. Instead of real tests or scripts,
# it uses echo commands to simulate the pipeline execution.
#
# A pipeline is composed of independent jobs that run scripts, grouped into stages.
# Stages run in sequential order, but jobs within stages run in parallel.
#
# For more information, see: https://docs.gitlab.com/ee/ci/yaml/index.html#stages

stages:          # List of stages for jobs, and their order of execution
  - info
  - test
  - build
  - push

env-dump:
  stage: info
  script:
    - export

lint-dockerfile-flexiwan:   # This job runs in the test stage.
  stage: test    # It can run at the same time as unit-test-job (in parallel).
  image: hadolint/hadolint:latest-alpine
  script: 
    - mkdir -p reports
    - hadolint -f gitlab_codeclimate docker/flexiwan/Dockerfile > reports/hadolint-$(md5sum docker/flexiwan/Dockerfile | cut -d" " -f1).json
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    reports:
      codequality:
        - "reports/*"
    paths:
      - "reports/*"

lint-dockerfile-mongo-sidecar:   # This job runs in the test stage.
  stage: test    # It can run at the same time as unit-test-job (in parallel).
  image: hadolint/hadolint:latest-alpine
  script: 
    - mkdir -p reports
    - hadolint -f gitlab_codeclimate docker/k8s-mongo-sidecar/Dockerfile > reports/hadolint-$(md5sum docker/k8s-mongo-sidecar/Dockerfile | cut -d" " -f1).json
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    reports:
      codequality:
        - "reports/*"
    paths:
      - "reports/*"

lint-dockerfile-sendria:   # This job runs in the test stage.
  stage: test    # It can run at the same time as unit-test-job (in parallel).
  image: hadolint/hadolint:latest-alpine
  script: 
    - mkdir -p reports
    - hadolint -f gitlab_codeclimate docker/sendria/Dockerfile > reports/hadolint-$(md5sum docker/sendria/Dockerfile | cut -d" " -f1).json
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    when: always
    reports:
      codequality:
        - "reports/*"
    paths:
      - "reports/*"

build-flexiwan-commit-sha:      # This job runs in the build stage.
  stage: build
  image: docker:latest
  variables:
    GIT_SUBMODULE_STRATEGY: normal
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    # fetches the latest image (not failing if image is not found)
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    # builds the project, passing proxy variables, using OCI labels
    # notice the cache-from, which is going to use the image we just pulled locally
    # the built image is tagged locally with the commit SHA, and then pushed to 
    # the GitLab registry
    - >
      docker build
      --pull
      --cache-from $CI_REGISTRY_IMAGE:latest
      --label "org.opencontainers.image.title=$CI_PROJECT_TITLE"
      --label "org.opencontainers.image.url=$CI_PROJECT_URL"
      --label "org.opencontainers.image.created=$CI_JOB_STARTED_AT"
      --label "org.opencontainers.image.revision=$CI_COMMIT_SHA"
      --label "org.opencontainers.image.version=$CI_COMMIT_REF_NAME"
      --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
      docker/flexiwan/
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA

# Here, the goal is to tag the "master" branch as "latest"
push-flexiwan-latest:
  image: docker:latest  
  stage: push
  variables:
    GIT_STRATEGY: none
  only:
    # Only "main" should be tagged "latest"
    - main
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    # Because we have no guarantee that this job will be picked up by the same runner 
    # that built the image in the previous step, we pull it again locally
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    # Then we tag it "latest"
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:latest
    # Annnd we push it.
    - docker push $CI_REGISTRY_IMAGE:latest

# Finally, the goal here is to Docker tag any Git tag
# GitLab will start a new pipeline everytime a Git tag is created, which is pretty awesome
push-flexiwan-tags:
  image: docker:latest
  stage: push
  variables:
    GIT_STRATEGY: none
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  only:
    # We want this job to be run on tags only.
    - tags
  script:
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME