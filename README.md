# k8s-flexiwan

## Description
Dockerfile and k8s manifest for standing up a minimal but functional Flexiwan SD-WAN Management Platform.

<!--## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.
-->

## Requirements
Flexiwan's Fleximanage currently has the following external system dependencies:
* A Redis server running v5.0.5
* A 3 node MongoDB [Replica Set](https://docs.mongodb.com/manual/tutorial/deploy-replica-set/) running v4.0.9
    * This will be automagically configured with the help of [k8s-mongodb-sidecar](https://github.com/morphy2k/k8s-mongo-sidecar)
* A mailer application or trapper
    * I've chosen [Sendria](https://github.com/msztolcman/sendria) for demonstration purposes
        * chosen as it is the successor to python mailtrap


## Installation
* Build Containers
```shell
docker build -t <username>/flexiwan docker/flexiwan
docker build -t <username>/k8s-mongodb-sidecar docker/k8s-mongodb-sidecar
docker build -t <username>/sendria docker/sendria
```  
* Upload Containers  
* Edit k8s yaml templates  

* Apply k8s configurations
```shell
kubectl apply -f k8s/{00-setup,01-mongo,02-redis,03-sendria,04-fleximanage}/
```

<!-- ## Usage -->

## Support
* Todo:
<!-- Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc. -->

## Roadmap
* Todo:
<!-- If you have ideas for releases in the future, it is a good idea to list them in the README. -->

<!-- ## Contributing -->

<!-- ## Authors and acknowledgment -->

## License
k8s-flexiwan is licensed under the [MIT License](LICENSE)

## Project status
Slowly being worked on in my free time.
