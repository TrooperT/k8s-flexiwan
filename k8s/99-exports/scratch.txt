"hosts" : [
        "mongo-0.mongo.fleximanage.svc.cluster.local:27017",
        "mongo-1.mongo.fleximanage.svc.cluster.local:27017",
        "mongo-2.mongo.fleximanage.svc.cluster.local:27017"
]
?replicaSet=rs0
combinedConfig.mongoUrl = process.env.MONGO_URL || combinedConfig.mongoUrl;
combinedConfig.mongoBillingUrl = process.env.MONGO_BILLING_URL || combinedConfig.mongoBillingUrl;
combinedConfig.mongoAnalyticsUrl = process.env.MONGO_ANALYTICS_URL || combinedConfig.mongoAnalyticsUrl;
combinedConfig.redisUrl = process.env.REDIS_URL || combinedConfig.redisUrl;

{
  mongoUrl: "mongodb://mongo-0.mongo.fleximanage.svc.cluster.local,mongo-1.mongo.fleximanage.svc.cluster.local,mongo-2.mongo.fleximanage.svc.cluster.local:27017/flexiwan?replicaSet=rs0",
  mongoBillingUrl: "mongodb://mongo-0.mongo.fleximanage.svc.cluster.local,mongo-1.mongo.fleximanage.svc.cluster.local,mongo-2.mongo.fleximanage.svc.cluster.local:27017/flexiwan?replicaSet=rs0"
  mongoAnalyticsUrl: 'mongodb://mongo-0.mongo.fleximanage.svc.cluster.local,mongo-1.mongo.fleximanage.svc.cluster.local,mongo-2.mongo.fleximanage.svc.cluster.local:27017/flexiwan?replicaSet=rs0'
  redisUrl: "redis://redis-master.fleximanage.svc.cluster.local:6379"
}
'sendria.fleximanage.svc.cluster.local'